{% extends "blogpost.html" %}

{% block title %}
<h1>Packaging models for production</h1>
{% endblock %}

{% block date %}
<p class="blogpost__date">Published on May 1, 2022</p>
{% endblock %}

{% block image %}
<img
  src="{{ url_for('static', filename='img/notebook_to_code.jpg') }}"
  alt="Notebook to code"
  class="blogpost__image" />
{% endblock %}

{% block article %}
    <p>Soledad Galli and Christopher Samiullah of <a href="https://www.trainindata.com/">trainindata</a> have a wonderful way of <a href="https://gitlab.com/tymyrddin/ml/-/tree/main/notebooks/preprocessing">structuring notebooks</a> which can easily be mapped to modules and packages. We have tried and played with it in the <a href="https://gitlab.com/tymyrddin/titanic/-/tree/main/production-model-package">titanic</a> and <a href="https://gitlab.com/tymyrddin/ames/-/tree/main/production-model-package">ames</a> training wheel repos for packaging the resulting R&D models.</p>
    <p>For the rest it is puzzling with configurations and bringing it together in a Tox config file.</p>

    <h2>Requirements</h2>
    <p>Following trainindata's structural setup for packaging, we use compatible release functionality (see <a href="https://www.python.org/dev/peps/pep-0440/#compatible-release">PEP 440</a>) to specify acceptable version ranges of project dependencies. This gives us the flexibility to keep up with small updates/fixes, whilst ensuring we don't install a major update which could introduce backwards incompatible changes.</p>
    <p>For now we just wish to get it all to work, so we are rather conservative and are not taking unnecessary risks in this packaging for the pipeline. Any changes beyond these limits will require a separate branch, to prevent breaking the build.</p>
    <p>There will be scenarios where testing is not necessary, so the requirements are split into to requirements files.</p>
    <p>The <code>requirements.txt</code>:</p>
    <pre><code>
        numpy>=1.20.0,<1.21.0
        pandas>=1.3.5,<1.4.0
        pydantic>=1.8.1,<1.9.0
        scikit-learn>=1.0.2,<1.1.0
        strictyaml>=1.3.2,<1.4.0
        ruamel.yaml==0.16.12
        feature-engine>=1.0.2,<1.1.0
        joblib>=1.0.1,<1.1.0
    </code></pre>

    <p>And the <code>test_requirements.txt</code>:</p>
    <pre><code>
        -r requirements.txt

        # testing requirements
        pytest>=6.2.3,<6.3.0

        # repo maintenance tooling
        black==20.8b1
        flake8>=3.9.0,<3.10.0
        mypy==0.812
        isort==5.8.0
    </code></pre>

    <p>Install without testing with:</p>

    <pre><code>
        $ pip install -r requirements/requirements.txt
    </code></pre>

    <p>Install all of it with:</p>

    <pre><code>
        $ pip install -r requirements/test_requirements.txt
    </code></pre>

    <p>Specifying version ranges for managing dependencies and keeping those stable, is a common best practice, whether one uses poetry, pipenv, or just pip, like here.</p>

    <h2>Package Configuration</h2>

    <p>The creation of configuration objects is done with a <code>config.yml</code> file, and <a href="https://hitchdev.com/strictyaml/why-not/turing-complete-code/">not with a python config (why not)</a>.</p>
    <p>All global constants from the jupyter notebooks are moved into a <a href="https://gitlab.com/tymyrddin/ames/-/blob/main/production-model-package/regression_model/config.yml">config.yml for ames</a> and <a href="https://gitlab.com/tymyrddin/titanic/-/blob/main/production-model-package/classification_model/config.yml">config.yml for titanic</a> in a yaml format. </p>
    <p>The <code>core.py</code> files can be found in the config directories. We are using <a href="https://pydantic-docs.helpmanual.io/">pydantic</a> for data validation and settings management (see <a href="https://peps.python.org/pep-0484/">PEP 484)</a>.</p>

    <h2>Tox</h2>

    <p><a href="https://tox.wiki/en/latest/">Tox</a> is a generic virtualenv management and test command line tool. Its goal is to standardize testing in Python.</p>

    <p>Using <a href="https://christophergs.com/python/2020/04/12/python-tox-why-use-it-and-tutorial/">Tox (why use it)</a> we can (on multiple operating systems):</p>
    <ul>
        <li>Eliminate PYTHONPATH challenges when running scripts/tests</li>
        <li>Eliminate virtual environment setup confusion</li>
        <li>Streamline steps such as model training and model publishing</li>
        <li>Reduce the use of shell scripts and <a href="https://mywiki.wooledge.org/BashPitfalls">its pitfalls</a></li>
    </ul>

    <p>All of Tox's configuration is in the <code>tox.ini</code> file:</p>
    <pre><code>
        [tox]
        envlist = test_package, typechecks, stylechecks, lint
        skipsdist = True

        [testenv]
        install_command = pip install {opts} {packages}

        [testenv:test_package]
        deps =
            -rrequirements/test_requirements.txt

        setenv =
            PYTHONPATH=.
            PYTHONHASHSEED=0

        commands=
            python regression_model/train_pipeline.py
            pytest \
            -s \
            -vv \
            {posargs:tests/}

        [testenv:train]
        envdir = {toxworkdir}/test_package
        deps =
            {[testenv:test_package]deps}

        setenv =
            {[testenv:test_package]setenv}

        commands=
            python regression_model/train_pipeline.py


        [testenv:typechecks]
        envdir = {toxworkdir}/test_package

        deps =
            {[testenv:test_package]deps}

        commands = {posargs:mypy regression_model}


        [testenv:stylechecks]
        envdir = {toxworkdir}/test_package

        deps =
            {[testenv:test_package]deps}

        commands = {posargs:flake8 regression_model tests}


        [testenv:lint]
        envdir = {toxworkdir}/test_package

        deps =
            {[testenv:test_package]deps}

        commands =
            isort regression_model tests
            black regression_model tests
            mypy regression_model
            flake8 regression_model

        [flake8]
        exclude = .git,env
        max-line-length = 90
    </code></pre>

    <h2>Example Tox commands</h2>

    <p>Make sure tox is installed (<code>pip install tox</code> or system wide <code>sudo apt install tox</code>), and the <code>train.csv</code> and  <code>test.csv</code> files are available in the <code>production-model-package/regression_model/datasets</code> directory when working with the <a href="https://gitlab.com/tymyrddin/ames/-/tree/main/production-model-package">ames</a> training wheel repo.</p>

    <p>To train the regression model (triggers the <code>train_pipeline.py</code> script):</p>
    <pre><code>
        $ tox -e train
        train installed: appdirs==1.4.4,attrs==21.4.0,black==20.8b1,click==8.1.3,feature-engine==1.0.2,flake8==3.9.2,iniconfig==1.1.1,isort==5.8.0,jobl
        ib==1.0.1,mccabe==0.6.1,mypy==0.812,mypy-extensions==0.4.3,numpy==1.20.3,packaging==21.3,pandas==1.3.5,pathspec==0.9.0,patsy==0.5.2,pluggy==1.0
        .0,py==1.11.0,pycodestyle==2.7.0,pydantic==1.8.2,pyflakes==2.3.1,pyparsing==3.0.8,pytest==6.2.5,python-dateutil==2.8.2,pytz==2022.1,regex==2022
        .4.24,ruamel.yaml==0.16.12,scikit-learn==1.0.2,scipy==1.8.0,six==1.16.0,statsmodels==0.13.2,strictyaml==1.3.2,threadpoolctl==3.1.0,toml==0.10.2
        ,typed-ast==1.4.3,typing_extensions==4.2.0
        train run-test-pre: PYTHONHASHSEED='0'
        train run-test: commands[0] | python regression_model/train_pipeline.py
        ___________________________________________________________________ summary ___________________________________________________________________
          train: commands succeeded
          congratulations :)
    </code></pre>
    <p>The <code>production-model-package/regression_model/trained_models</code> directory now contains a <code>regression_model_output_v0.0.1.pkl</code> pickle file.</p>

    <p>To run the <code>test_package</code> environment:</p>
    <pre><code>
        $ tox -e test_package
        test_package installed: [snip]
        test_package run-test-pre: PYTHONHASHSEED='0'
        test_package run-test: commands[0] | python regression_model/train_pipeline.py
        test_package run-test: commands[1] | pytest -s -vv tests/
        ============================================================= test session starts =============================================================
        platform linux -- Python 3.9.5, pytest-6.2.5, py-1.11.0, pluggy-1.0.0 -- /path/to/production-model-package/.tox/test_
        package/bin/python
        cachedir: .tox/test_package/.pytest_cache
        rootdir: /home/nina/Development/gitlab/ames/production-model-package, configfile: pyproject.toml
        collected 2 items

        tests/test_features.py::test_temporal_variable_transformer PASSED
        tests/test_prediction.py::test_make_prediction PASSED

        ============================================================== 2 passed in 0.19s ==============================================================
        ___________________________________________________________________ summary ___________________________________________________________________
          test_package: commands succeeded
          congratulations :)
    </code></pre>

    <p>To run all:</p>
    <pre><code>
        $ tox
        test_package installed: [snip]
        test_package run-test-pre: PYTHONHASHSEED='0'
        test_package run-test: commands[0] | python regression_model/train_pipeline.py
        test_package run-test: commands[1] | pytest -s -vv tests/
        ============================================================= test session starts =============================================================
        platform linux -- Python 3.9.5, pytest-6.2.5, py-1.11.0, pluggy-1.0.0 -- /path/to/production-model-package/.tox/test_package/bin/python
        cachedir: .tox/test_package/.pytest_cache
        rootdir: /home/nina/Development/gitlab/ames/production-model-package, configfile: pyproject.toml
        collected 2 items

        tests/test_features.py::test_temporal_variable_transformer PASSED
        tests/test_prediction.py::test_make_prediction PASSED

        ============================================================== 2 passed in 0.18s ==============================================================
        typechecks installed: [snip]
        typechecks run-test-pre: PYTHONHASHSEED='4267597864'
        typechecks run-test: commands[0] | mypy regression_model
        Success: no issues found in 12 source files
        stylechecks installed: [snip]
        stylechecks run-test-pre: PYTHONHASHSEED='4267597864'
        stylechecks run-test: commands[0] | flake8 regression_model tests
        lint installed: [snip]
        lint run-test-pre: PYTHONHASHSEED='4267597864'
        lint run-test: commands[0] | isort regression_model tests
        lint run-test: commands[1] | - black classification_model tests
        Traceback (most recent call last):[snip]
        ImportError: cannot import name '_unicodefun' from 'click' [snip]
        lint run-test: commands[2] | mypy regression_model
        Success: no issues found in 12 source files
        lint run-test: commands[3] | flake8 regression_model
        ___________________________________________________________________ summary ___________________________________________________________________
          test_package: commands succeeded
          typechecks: commands succeeded
          stylechecks: commands succeeded
          lint: commands succeeded
          congratulations :)
    </code></pre>

    <h2>Troubleshooting Tox</h2>

    <p>The <code>ImportError: cannot import name '_unicodefun' from 'click'</code> is a known issue. Upgrading <code>black==22.3.0</code> and downgrading <code>click==8.0.2</code> helps for manually running the checks, but not via Tox. Hence, the dash in front of black. It will run and give the error but will not block the process.</p>
    <p>If for some reason you are unable to run things with Tox, then you can run the Python commands listed in the tox.ini file by hand. To do this, add the <code>production-model-package</code> directory to the system <code>PYTHONPATH</code>:</p>

    <pre><code>
        $ pwd
        /path/to/production-model-package
    </code></pre>

    <p>Then, add the path to your <code>~/.bashrc</code>:</p>
    <pre><code>
        $ export PYTHONPATH="${PYTHONPATH}:/path/to/production-model-package"
    </code></pre>

    <h2>Building the package</h2>
    <p><a href="https://packaging.python.org/en/latest/tutorials/packaging-projects/">Packaging</a> uses <code>MANIFEST.in</code>, <code>pyproject.toml</code> and a <code>setup.py</code>. All three are often adaptations from existing templates, or are generated by yet another tool. A <a href="https://gitlab.com/tymyrddin/ames/-/blob/main/production-model-package/MANIFEST.in">MANIFEST.in</a> file specifies which files are to be included in the package, and the <code>setup.py</code> is where the <a href="https://gitlab.com/tymyrddin/ames/-/blob/main/production-model-package/setup.py">magic happens</a>.</p>

    <p>To build the package, install <code>build</code>, and run this command from the same directory where <code>pyproject.toml</code> is located:</p>
    <pre><code>
        $ python -m pip install --upgrade build
        [snip]
        $ python -m build
        [snip]
        Successfully built ames-regression-model-0.0.1.tar.gz and ames_regression_model-0.0.1-py3-none-any.whl
    </code></pre>

    <p>A <code>build</code>, <code>egg.info</code> and <code>dist</code> directory will appear. These are for installing the package in the <code>api</code>application.</p>

{% endblock %}
