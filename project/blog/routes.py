from . import blog_blueprint
from flask import render_template, abort


blog_post_titles = [
    "packaging-models",
    "chaotic-order",
    "ml-ops",
    "security-scanning",
    "code-quality",
    "moon",
    "ml-pain-points",
]


@blog_blueprint.route("/blog/")
def blog():
    return render_template("blog/blog.html")


@blog_blueprint.route("/<blog_title>/")
def posts(blog_title):
    if blog_title not in blog_post_titles:
        abort(404)

    return render_template(f"blog/{blog_title}.html")


@blog_blueprint.route("/about/")
def about():
    return render_template("blog/about.html")


@blog_blueprint.route("/reservations/")
def reservations():
    return render_template("blog/reservations.html")
