from . import recipes_blueprint
from flask import render_template


titanic_recipes_names = [
    "titanic_notebooks",
    "titanic_packaging",
    "titanic_api",
    "titanic_cicd",
    "titanic_container",
    "titanic_testing",
    "titanic_aws",
]
ames_recipes_names = [
    "ames_notebooks",
    "ames_packaging",
    "ames_api",
    "ames_cicd",
    "ames_container",
    "ames_testing",
    "ames_aws",
]
nlp_recipes_names = ["setup", "docs", "lda", "cai", "connect", "rekognition"]
data_visualisation_recipes_names = ["matplotlib", "seaborn", "geoplots", "bokeh"]
deep_learning_recipes_names = ["nn", "cnns", "nltk", "rnns", "advanced_rnns", "gans"]
data_analysis_recipes_names = [
    "bike_sharing",
    "absenteism",
    "marketing",
    "bankruptcy",
    "shoppers",
    "credit_cards",
    "heart",
    "retail",
    "energy",
    "air_quality",
]
ml_recipes_names = [
    "neural_networks",
    "supervised_metrics",
    "preprocessing",
    "unsupervised_algorithms",
    "supervised_algorithms",
]
data_wrangling_recipes_names = ["diving", "sources", "secrets", "scraping", "databases"]
mlops_recipes_names = [
    "titanic",
    "ames",
    "roadmaps",
]


@recipes_blueprint.route("/")
def recipes():
    return render_template("recipes/recipes.html")


@recipes_blueprint.route("/nlp/")
def nlp_recipes():
    return render_template("recipes/nlp.html")


@recipes_blueprint.route("/data_visualisation/")
def data_visualisation_recipes():
    return render_template("recipes/data_visualisation.html")


@recipes_blueprint.route("/deep_learning/")
def deep_learning_recipes():
    return render_template("recipes/deep_learning.html")


@recipes_blueprint.route("/data_analysis/")
def data_analysis_recipes():
    return render_template("recipes/data_analysis.html")


@recipes_blueprint.route("/data_wrangling/")
def data_wrangling_recipes():
    return render_template("recipes/data_wrangling.html")


@recipes_blueprint.route("/ml/")
def ml_recipes():
    return render_template("recipes/ml.html")


@recipes_blueprint.route("/mlops/")
def mlops_recipes():
    return render_template("recipes/mlops.html")


@recipes_blueprint.route("/titanic/")
def titanic_recipes():
    return render_template("recipes/titanic.html")


@recipes_blueprint.route("/ames/")
def ames_recipes():
    return render_template("recipes/ames.html")


@recipes_blueprint.route('/404/')
def fourohfour_recipes():
    return render_template('recipes/404.html')
