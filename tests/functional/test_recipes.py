"""
This file (test_recipes.py) contains the functional tests for the `recipes` blueprint.
"""
from project.recipes.routes import (
    titanic_recipes_names,
    ames_recipes_names,
    nlp_recipes_names,
    data_visualisation_recipes_names,
    deep_learning_recipes_names,
    data_analysis_recipes_names,
    ml_recipes_names,
    data_wrangling_recipes_names,
)


def test_get_home_page(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (GET)
    THEN check the response is valid
    """
    header_items = [
        b"Ty Myrddin Home",
        b"Milliways",
        b"Cuisine Starlight Blog",
        b"About",
        b"Make a reservation",
    ]
    recipe_types = [
        b"Natural Language Processing",
        b"Data visualisation",
        b"Deep learning",
        b"Data analysis",
        b"Machine learning",
        b"Data Wrangling",
        b"MLOps",
    ]
    response = test_client.get("/")
    assert response.status_code == 200
    for header_item in header_items:
        assert header_item in response.data
    for recipe_type in recipe_types:
        assert recipe_type in response.data


def test_get_data_wrangling_recipes(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/data_wrangling/' page is requested (GET)
    THEN check the response is valid
    """
    recipes = [
        b"Plunging in",
        b"Data sources",
        b"Some secrets",
        b"Data gathering",
        b"Databases",
    ]
    response = test_client.get("/data_wrangling/")
    assert response.status_code == 200
    for recipe in recipes:
        assert recipe in response.data


def test_get_invalid_individual_recipes(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/<recipe_type>>/<recipe_name>' page is requested (GET) with invalid recipe names
    THEN check that 404 errors are returned
    """
    invalid_recipe_names = ["acai_bowls", "french_toast", "breakfast_burrito", "abcd"]
    for recipe_name in invalid_recipe_names:
        response = test_client.get(f"/titanic/{recipe_name}/")
        assert response.status_code == 404
        response = test_client.get(f"/nlp/{recipe_name}/")
        assert response.status_code == 404
        response = test_client.get(f"/data_visualisation/{recipe_name}/")
        assert response.status_code == 404
        response = test_client.get(f"/deep_learning/{recipe_name}/")
        assert response.status_code == 404
        response = test_client.get(f"/data_analysis/{recipe_name}/")
        assert response.status_code == 404
        response = test_client.get(f"/ml/{recipe_name}/")
        assert response.status_code == 404
        response = test_client.get(f"/data_wrangling/{recipe_name}/")
        assert response.status_code == 404


def test_get_data_analysis_recipes(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/data_analysis/' page is requested (GET)
    THEN check the response is valid
    """
    recipes = [
        b"Energy consumed by appliances",
        b"Analyzing air quality",
        b"Online retail",
        b"Heart disease",
        b"Credit card defaulters",
    ]
    response = test_client.get("/data_analysis/")
    assert response.status_code == 200
    for recipe in recipes:
        assert recipe in response.data


def test_get_data_visualisation_recipes(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/data_visualisation/' page is requested (GET)
    THEN check the response is valid
    """
    recipes = [b"Matplotlib", b"Seaborn", b"Bokeh"]
    response = test_client.get("/data_visualisation/")
    assert response.status_code == 200
    for recipe in recipes:
        assert recipe in response.data


def test_get_nlp_recipes(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/nlp/' page is requested (GET)
    THEN check the response is valid
    """
    recipes = [b"Set up services", b"Analysing documents and text"]
    response = test_client.get("/nlp/")
    assert response.status_code == 200
    for recipe in recipes:
        assert recipe in response.data


def test_get_deep_learning_recipes(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/deep_learning/' page is requested (GET)
    THEN check the response is valid
    """
    recipes = [
        b"Traditional Neural Networks",
        b"Recurrent Neural Networks",
        b"LSTMs, GRUs, and Advanced RNNs",
        b"Generative Adversarial Networks",
    ]
    response = test_client.get("/deep_learning/")
    assert response.status_code == 200
    for recipe in recipes:
        assert recipe in response.data


def test_get_ml_recipes(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/ml/' page is requested (GET)
    THEN check the response is valid
    """
    recipes = [
        b"Data preprocessing",
        b"Unsupervised learning algorithms",
        b"Supervised learning metrics",
    ]
    response = test_client.get("/ml/")
    assert response.status_code == 200
    for recipe in recipes:
        assert recipe in response.data
